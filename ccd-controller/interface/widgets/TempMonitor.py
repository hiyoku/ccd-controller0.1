
from PyQt5.QtWidgets import QWidget, QLabel


from core.main.SchedTemperature import SchedTemperature
from core.main.Locker import Locker


class TempMonitor(QWidget):
    lock = Locker()

    def __init__(self, parent=None):
        super(TempMonitor, self).__init__(parent)
        self.init_widgets()

    def init_widgets(self):
        self.TempMonitor = QLabel(self)
        self.TempMonitor.resize(75, 25)
        self.TempMonitor.move(70, 0)

        self.label = QLabel(self)
        self.label.setText("Temperatura:")
        self.label.move(0, 5)

        self.Sched = SchedTemperature(self.TempMonitor)

    def stop_monitor(self):
        self.Sched.stop_job()

    def start_monitor(self):
        self.Sched.start_job()
