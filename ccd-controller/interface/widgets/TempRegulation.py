
from PyQt5.QtWidgets import QTextEdit, QWidget, QPushButton

from core.ccd import SbigDriver
from core.main.Locker import Locker


class TempRegulation(QWidget):
    lock = Locker()

    def __init__(self, parent=None):
        super(TempRegulation, self).__init__(parent)
        self.init_widgets()

    def init_widgets(self):
        self.setBtn = QPushButton("Set Temp.", self)
        self.setBtn.clicked.connect(self.btn_temperature)
        self.setBtn.resize(75, 25)

        self.setField = QTextEdit(self)
        self.setField.resize(50, 25)
        self.setField.move(80, 0)

    def btn_temperature(self):
        try:
            value = self.setField.toPlainText()
            if value is '': value = 20
            self.set_temperature(float(value))
        except Exception as e:
            print("Exception -> {}".format(e))

    def set_temperature(self, value):
        self.lock.set_acquire()
        try:
            SbigDriver.set_temperature(regulation=True, setpoint=value, autofreeze=False)
        except Exception as e:
            print("Exception: {}".format(e))
        finally:
            self.lock.set_release()
