from PyQt5.QtWidgets import QWidget, QPushButton, QTextEdit, QLabel

from core.main.SThread import SThread


class Shooter(QWidget):
    """
        Class for Taking photo Widget
    """

    def __init__(self, parent=None):
        super(Shooter, self).__init__(parent)
        self.init_widgets()

    def init_widgets(self):
        self.tb = QTextEdit(self)
        self.tb.resize(50, 25)
        self.tb.move(80, 0)

        self.pLabel = QLabel(self)
        self.pLabel.setText("Prefixo:")
        self.pLabel.move(150, 5)

        self.pre = QTextEdit(self)
        self.pre.resize(75, 25)
        self.pre.move(190, 0)

        self.sbutton = QPushButton("Shot!", self)
        self.sbutton.clicked.connect(self.shoot)
        self.sbutton.resize(75, 25)

    def shoot(self):
        etime = int(self.tb.toPlainText())
        pre = self.pre.toPlainText()
        self.ss = SThread(etime, pre)
        self.ss.start()
