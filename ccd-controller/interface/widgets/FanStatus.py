from PyQt5.QtWidgets import QWidget, QTextEdit, QPushButton

from core.main.Locker import Locker
from core.ccd import SbigDriver


class FanStatus(QWidget):
    lock = Locker()

    def __init__(self, parent=None):
        super(FanStatus, self).__init__(parent)
        self.init_widgets()

    def init_widgets(self):
        self.FanField = QTextEdit(self)
        self.FanField.setText(self.fan_status())
        self.FanField.setReadOnly(True)
        self.FanField.move(50, 0)

        self.FanButton = QPushButton("Fan: ", self)
        self.FanButton.clicked.connect(self.set_fan)
        self.FanButton.resize(50, 25)

    def fan_status(self):
        self.lock.set_acquire()
        status = SbigDriver.is_fanning()
        self.lock.set_release()

        return "ON" if status else "OFF"

    def set_fan(self):
        self.lock.set_acquire()
        try:
            if SbigDriver.is_fanning():
                SbigDriver.stop_fan()
            else:
                SbigDriver.start_fan()
        finally:
            self.FanField.setText(self.fan_status())
            self.lock.set_release()