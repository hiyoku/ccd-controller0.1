from PyQt5.QtWidgets import QWidget, QLabel, QTextEdit

from core.ccd import SbigDriver
from core.main.Locker import Locker

class CCDInfo(QWidget):
    lock = Locker()
    def __init__(self, parent=None):
        super(CCDInfo, self).__init__(parent)
        self.init_widgets()

    def init_widgets(self):
        info = self.get_info()

        # Camera Firmware
        self.lf = QLabel(self)
        self.lf.setText("Firmware: ")
        self.lf.move(0, 20)

        self.tfirm = QTextEdit(self)
        self.tfirm.move(50, 15)
        self.tfirm.resize(75, 25)
        self.tfirm.setPlainText(str(info[0]))
        self.tfirm.setReadOnly(True)

        # Camera Name
        self.ln = QLabel(self)
        self.ln.setText("Camera: ")
        self.ln.move(0, 50)

        self.cn = QTextEdit(self)
        self.cn.setText(str(info[2])[2:len(str(info[2]))-1])
        self.cn.setReadOnly(True)
        self.cn.resize(200, 25)
        self.cn.move(40, 45)

    def get_info(self):
        """
            This function will return [CameraFirmware, Camera Type, Camera Name]
        """
        ret = None
        self.lock.set_acquire()
        try:
            ret = tuple(SbigDriver.ccdinfo())
        except Exception as e:
            print("Exception -> {}".format(e))
        finally:
            self.lock.set_release()
            return ret

