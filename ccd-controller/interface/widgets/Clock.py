#! /usr/bin/python3
from time import strftime

from apscheduler.schedulers.background import BackgroundScheduler
from PyQt5.QtWidgets import QLCDNumber, QWidget


class Clock(QWidget):
    
    def __init__(self, parent=None):
        super(Clock, self).__init__(parent)
        self.lcd = QLCDNumber(self)
        self.lcd.setDigitCount(8)
        self.lcd.display(strftime('%H:%M:%S'))
        self.lcd.setGeometry(40, 50, 220, 40)
        self.start()

    # Functions for Watch
    # Scheduling a new refresh
    def start(self):
        scheduler = BackgroundScheduler()
        scheduler.add_job(self.refresh, 'interval', seconds=0.2)
        scheduler.start()

    def refresh(self):
        self.lcd.display(strftime('%H:%M:%S'))
