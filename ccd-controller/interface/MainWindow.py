import sys

from PyQt5.QtWidgets import (QMainWindow, QAction,
                             qApp, QApplication,
                             QPushButton, QTextEdit,
                             QLabel)
from PyQt5.QtGui import QIcon

from interface.widgets.Clock import Clock
from interface.widgets.CCDInfo import CCDInfo
from interface.widgets.FanStatus import FanStatus
from interface.widgets.TempMonitor import TempMonitor
from interface.widgets.TempRegulation import TempRegulation
from interface.widgets.Shooter import Shooter


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui()

    def ui(self):
        # Adding the widgets
        # MenuBar
        self.menu()

        # Clock Widget
        self.clock = Clock(self)
        self.clock.resize(300, 100)
        self.clock.move(100, 30)

        # Fan Widgets
        self.fan_status = FanStatus(self)
        self.fan_status.resize(100, 25)
        self.fan_status.move(150, 250)

        # Temperature Monitor Widgets
        self.temp_monitor = TempMonitor(self)
        self.temp_monitor.resize(200, 25)
        self.temp_monitor.move(280, 250)

        # Temperature Regulation Widgets
        self.temp_reg = TempRegulation(self)
        self.temp_reg.resize(150, 25)
        self.temp_reg.move(150, 300)

        # Photo Widgets
        self.shooter = Shooter(self)
        self.shooter.resize(300, 25)
        self.shooter.move(100, 150)

        # CCD Informations Widgets
        self.ccdinfo = CCDInfo(self)
        self.ccdinfo.move(120, 175)
        self.ccdinfo.resize(300, 70)


        self.status("Ready!")

        # Creating the MainWindow
        self.setGeometry(500, 100, 500, 400)
        self.setWindowTitle('Schedule Window')
        self.show()

    def menu(self):
        # Creating Menu to quit
        aexit = QAction(QIcon('icons/exit.png'), '&Exit', self)
        aexit.setStatusTip('Exit application')
        aexit.triggered.connect(qApp.quit)

        # Creating the menuBar
        menubar = self.menuBar()

        mfile = menubar.addMenu('&File')
        mfile.addAction(aexit)

    def status(self, m):
        self.statusBar().showMessage(m)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())

