from PyQt5.QtCore import QThread

from core.main.Locker import Locker
from core.ccd import SbigDriver
from core.main.SchedTemperature import SchedTemperature

class SThread(QThread):
    lock = Locker()

    def __init__(self, etime, pre, parent=None):
        super(SThread, self).__init__(parent)
        self.etime = etime
        self.pre = pre
        self.Sched = SchedTemperature(self)

    def run(self):
        # print("Shoot", id(self.lock), self.lock.printID())
        # print("Temp:", id(self.Sched), self.Sched.printID())
        self.Sched.stop_job()
        self.lock.set_acquire()
        try:
            SbigDriver.auto(self.etime * 100, self.pre)
        except Exception as e:
            print("Exception -> {}".format(e))
        finally:
            self.lock.set_release()
            self.Sched.start_job()